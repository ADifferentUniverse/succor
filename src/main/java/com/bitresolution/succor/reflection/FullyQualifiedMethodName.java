package com.bitresolution.succor.reflection;

import com.google.common.base.Objects;

import java.lang.reflect.Method;

public class FullyQualifiedMethodName {

    private final FullyQualifiedClassName className;
    private final String methodName;
    private final Class<?>[] arguments;

    public FullyQualifiedMethodName(FullyQualifiedClassName className, String methodName, Class<?>[] arguments) {
        this.className = className;
        this.methodName = methodName;
        this.arguments = arguments;
    }

    public FullyQualifiedMethodName(Method method) {
        this(new FullyQualifiedClassName(method.getDeclaringClass()), method.getName(), method.getParameterTypes());
    }

    public String getMethodName() {
        return methodName;
    }

    public FullyQualifiedClassName getFullyQualifiedClassName() {
        return className;
    }

    public PackageName getPackageName() {
        return className.getPackageName();
    }

    public String getFullyQualifiedName() {
        return String.format("%s#%s", className.getFullyQualifiedName(), methodName);
    }

    public Method getMethod() throws ClassNotFoundException, NoSuchMethodException {
        return className.loadClass().getMethod(methodName);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(className, methodName);
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        if(obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final FullyQualifiedMethodName other = (FullyQualifiedMethodName) obj;
        return Objects.equal(this.className, other.className) && Objects.equal(this.methodName, other.methodName);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("className", className)
                .add("methodName", methodName)
                .toString();
    }
}
