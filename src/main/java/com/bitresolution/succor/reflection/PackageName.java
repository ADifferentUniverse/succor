package com.bitresolution.succor.reflection;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import org.apache.commons.lang3.ArrayUtils;

import java.util.Arrays;

public class PackageName {

    public static final PackageName DEFAULT = new PackageName();

    private static final Joiner joiner = Joiner.on(".");
    private static final Joiner JOINER = Joiner.on(".");

    private final String[] parts;

    public PackageName() {
        this(new String[0]);
    }

    public PackageName(String packageName) {
        this(parse(packageName));
    }

    public PackageName(String[] parts) {
        this.parts = parts;
    }

    private static String[] parse(String packageName) {
        if(Strings.isNullOrEmpty(packageName))
            return DEFAULT.parts;
        else
            return packageName.lastIndexOf(".") == -1 ? new String[]{packageName} : packageName.split("\\.");
    }

    public boolean isDefaultPackage() {
        return parts.length == 0;
    }

    public PackageName getParent() {
        return new PackageName(Arrays.copyOfRange(parts, 0, parts.length == 0 ? 0 : parts.length - 1));
    }

    public PackageName getChild(String name) {
        return new PackageName(ArrayUtils.add(parts, name));
    }

    public String[] getParts() {
        return parts;
    }

    public String getName() {
        return joiner.join(parts);
    }

    @Override
    @SuppressWarnings("vararg")
    public int hashCode() {
        return Arrays.hashCode(parts);
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        if(obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final PackageName other = (PackageName) obj;
        return Arrays.equals(this.parts, other.parts);
    }

    @Override
    public String toString() {
        return JOINER.join(parts);
    }
}
