package com.bitresolution.succor.collections;

public interface Factory<T> {

    T create();
}
