package com.bitresolution.succor.spring;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;

public class GracefulShutdownExecutorService implements ExecutorService {

    private final ExecutorService executor;
    private final long duration;
    private final TimeUnit unit;

    public GracefulShutdownExecutorService(ExecutorService executor, long duration, TimeUnit unit) {
        this.executor = executor;
        this.duration = duration;
        this.unit = unit;
    }

    @Override
    public void shutdown() {
        executor.shutdown();
        try {
            awaitTermination(duration, unit);
        }
        catch (InterruptedException e) {
            throw new RuntimeException("Timeout shutting down executor service: " + executor, e);
        }
    }

    @Override
    public List<Runnable> shutdownNow() {
        return executor.shutdownNow();
    }

    @Override
    public boolean isShutdown() {
        return executor.isShutdown();
    }

    @Override
    public boolean isTerminated() {
        return executor.isTerminated();
    }

    @Override
    public boolean awaitTermination(long l, TimeUnit timeUnit) throws InterruptedException {
        return executor.awaitTermination(l, timeUnit);
    }

    @Override
    public <T> Future<T> submit(Callable<T> tCallable) {
        return executor.submit(tCallable);
    }

    @Override
    public <T> Future<T> submit(Runnable runnable, T t) {
        return executor.submit(runnable, t);
    }

    @Override
    public Future<?> submit(Runnable runnable) {
        return executor.submit(runnable);
    }

    @Override
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> callables) throws InterruptedException {
        return executor.invokeAll(callables);
    }

    @Override
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> callables, long l, TimeUnit timeUnit) throws InterruptedException {
        return executor.invokeAll(callables, l, timeUnit);
    }

    @Override
    public <T> T invokeAny(Collection<? extends Callable<T>> callables) throws InterruptedException, ExecutionException {
        return executor.invokeAny(callables);
    }

    @Override
    public <T> T invokeAny(Collection<? extends Callable<T>> callables, long l, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        return executor.invokeAny(callables, l, timeUnit);
    }

    @Override
    public void execute(Runnable runnable) {
        executor.execute(runnable);
    }
}
