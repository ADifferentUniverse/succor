package com.bitresolution.succor.junit.runners.specs.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for a scenario which may have cases injected if defined as a single
 * argument method signature of type Enum.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Scenario {
    String name() default "";
}
