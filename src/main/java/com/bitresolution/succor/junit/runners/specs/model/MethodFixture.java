package com.bitresolution.succor.junit.runners.specs.model;

import java.lang.reflect.Method;
import java.util.List;

/**
 *
 */
public abstract class MethodFixture extends BaseFixture {

    protected final Method method;

    public MethodFixture(List<? extends Fixture> fixtures, Method method) {
        super(fixtures);
        this.method = method;
    }

    public Method getMethod() {
        return method;
    }
}
