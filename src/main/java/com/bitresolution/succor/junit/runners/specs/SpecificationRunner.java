package com.bitresolution.succor.junit.runners.specs;

import com.bitresolution.succor.junit.runners.specs.model.SpecificationFixture;
import com.bitresolution.succor.junit.runners.specs.transform.*;
import org.junit.runner.Description;
import org.junit.runner.Runner;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.model.InitializationError;

/**
 * JUnitSpecs default runner
 */
public class SpecificationRunner extends Runner {

    private SpecificationFixture specification;
    private SpecificationFactory factory;
    private DescriptionReducer describer;
    private ExecutionReducer executor;

    public SpecificationRunner(Class<?> testClass) throws InitializationError {
        this.factory = new DefaultSpecificationFactory();
        this.specification = createSpecification(testClass);
        this.describer = new CachingDescriptionReducer(new DefaultDescriptionReducer());
        this.executor = new DefaultExecutionReducer(describer);
    }

    public SpecificationRunner(Class<?> testClass, SpecificationFactory factory, DescriptionReducer describer, ExecutionReducer executor) throws InitializationError {
        this.factory = factory;
        this.specification = createSpecification(testClass);
        this.describer = describer;
        this.executor = executor;
    }

    protected SpecificationFixture createSpecification(Class<?> testClass) throws InitializationError {
        return factory.from(testClass);
    }

    @Override
    public Description getDescription() {
        final Description description = describer.visit(specification);
        return description;
    }

    @Override
    public void run(RunNotifier notifier) {
        executor.with(notifier).visit(specification);
    }
}
