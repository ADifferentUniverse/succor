package com.bitresolution.succor.junit.runners.specs.transform;

import org.junit.runner.Description;

/**
 *
 */
public interface DescriptionReducer extends FixtureVisitor<Description> {
}
