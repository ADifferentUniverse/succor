package com.bitresolution.succor.junit.runners.specs.model;

import com.bitresolution.succor.junit.runners.specs.transform.FixtureVisitor;
import com.bitresolution.succor.junit.runners.specs.util.Primes;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Iterator;

/**
 * An executable Case test
 */
public class CaseFixture extends MethodFixture {

    private final Enum scenarioCase;

    public CaseFixture(Method method, Enum scenarioCase) {
        super(Collections.<Fixture>emptyList(), method);
        this.scenarioCase = scenarioCase;
    }

    public Enum getCase() {
        return scenarioCase;
    }

    @Override
    public <T> T accept(FixtureVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CaseFixture that = (CaseFixture) o;

        if (scenarioCase != null ? !scenarioCase.equals(that.scenarioCase) : that.scenarioCase != null) return false;
        if (method != null && that.method != null) {
            if (!method.equals(that.method)) return false;
        }
        else {
            if (!(method == null && that.method == null)) return false;
        }
        if (fixtures != null ? !fixtures.equals(that.fixtures) : that.fixtures != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = 0;
        final Iterator<Integer> prime = Primes.fromOffsetGetPrimesAndCycle(31, 3);
        result = prime.next() * result + (scenarioCase != null ? scenarioCase.hashCode() : 0);
        result = prime.next() * result + (method != null ? method.hashCode() : 0);
        result = prime.next() * result + (fixtures != null ? fixtures.hashCode() : 0);
        return result;
    }
}
