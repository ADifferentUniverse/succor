package com.bitresolution.succor.junit.runners.specs.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public final class AnnotationUtils {

    private AnnotationUtils() {
        throw new IllegalStateException("Utils class can not be instantiated");
    }

    public static Annotation getAnnotation(Class<? extends Annotation> annotation, Class<?> source) {
        return getAnnotation(annotation, source, new ArrayList<Class<?>>());
    }

    public static Annotation getAnnotation(Class<? extends Annotation> query, Method method) {
        List<Class<?>> visited = new ArrayList<Class<?>>();
        final Annotation[] annotations = method.getAnnotations();
        for (Annotation annotation : annotations) {
            final Annotation result = getAnnotation(query, annotation, visited);
            if (result != null)
                return result;
        }
        return null;
    }

    private static Annotation getAnnotation(Class<? extends Annotation> query, Class<?> source, List<Class<?>> visited) {
        if (visited.contains(source)) {
            return null;
        }
        visited.add(source);
        for (Annotation annotation : source.getAnnotations()) {
            final Annotation result = getAnnotation(query, annotation, visited);
            if (result != null)
                return result;
        }
        return null;
    }

    private static Annotation getAnnotation(Class<? extends Annotation> query, Annotation annotation, List<Class<?>> visited) {
        if (annotation.annotationType().equals(query)) {
            return annotation;
        }
        else
            return getAnnotation(query, annotation.annotationType(), visited);
    }
}
