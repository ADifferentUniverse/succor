package com.bitresolution.succor.junit.runners.specs.model;

import java.util.List;

/**
 *
 */
public abstract class BaseFixture implements Fixture {

    protected final List<? extends Fixture> fixtures;

    public BaseFixture(List<? extends Fixture> fixtures) {
        this.fixtures = fixtures;
    }

    public List<? extends Fixture> getFixtures() {
        return fixtures;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" +
                ", fixtures=" + fixtures +
                '}';
    }
}
