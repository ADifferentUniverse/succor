package com.bitresolution.succor.junit.runners.specs.util;

import com.google.common.collect.Iterators;

import java.util.Iterator;
import java.util.List;

/**
 *
 */
public class Primes {

    private Primes() {
        throw new IllegalStateException("Primes class can not be instantiated");
    }

    public static Iterator<Integer> fromOffsetGetPrimesAndCycle(int start, int count) {
        final List<Integer> primes = new PrimeNumberGenerator().primes(count, start);
        return Iterators.cycle(primes);
    }
}
