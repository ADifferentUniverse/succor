package com.bitresolution.succor.junit.runners.specs.transform;

import com.bitresolution.succor.junit.runners.specs.model.CaseFixture;
import com.bitresolution.succor.junit.runners.specs.model.Fixture;
import com.bitresolution.succor.junit.runners.specs.model.ScenarioFixture;
import com.bitresolution.succor.junit.runners.specs.model.SpecificationFixture;
import org.junit.runner.Description;

/**
 *
 */
public class DefaultDescriptionReducer implements DescriptionReducer {

    public Description visit(Fixture fixture) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Description visit(SpecificationFixture fixture) {
        final Description description = Description.createSuiteDescription(fixture.getTestClass());
        return addChildFixtureDescriptions(fixture, description);
    }

    @Override
    public Description visit(ScenarioFixture fixture) {
        if (fixture.getFixtures().isEmpty()) {
            return Description.createTestDescription(fixture.getMethod().getDeclaringClass(), fixture.getMethod().getName(), fixture.getMethod().getAnnotations());
        }
        else {
            final Description description = Description.createSuiteDescription(fixture.getMethod().getName(), fixture.getMethod().getAnnotations());
            return addChildFixtureDescriptions(fixture, description);
        }
    }

    @Override
    public Description visit(CaseFixture fixture) {
        return Description.createTestDescription(fixture.getMethod().getDeclaringClass(), fixture.getCase().name(), fixture.getMethod().getAnnotations());
    }

    private Description addChildFixtureDescriptions(Fixture fixture, Description description) {
        for (Fixture f : fixture.getFixtures()) {
            description.addChild(f.accept(this));
        }
        return description;
    }
}
