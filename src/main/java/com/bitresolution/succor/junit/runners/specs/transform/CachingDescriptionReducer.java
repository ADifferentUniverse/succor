package com.bitresolution.succor.junit.runners.specs.transform;

import com.bitresolution.succor.junit.runners.specs.model.CaseFixture;
import com.bitresolution.succor.junit.runners.specs.model.Fixture;
import com.bitresolution.succor.junit.runners.specs.model.ScenarioFixture;
import com.bitresolution.succor.junit.runners.specs.model.SpecificationFixture;
import org.junit.runner.Description;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class CachingDescriptionReducer implements DescriptionReducer {

    private Map<Fixture, Description> cache;
    private DescriptionReducer reducer;

    public CachingDescriptionReducer(DescriptionReducer reducer) {
        this.cache = new HashMap<Fixture, Description>();
        this.reducer = reducer;
    }

    public Description visit(Fixture fixture) {
        if (cache.containsKey(fixture)) {
            return cache.get(fixture);
        }
        else {
            final Description description = fixture.accept(reducer);
            cache.put(fixture, description);
            return description;
        }
    }

    @Override
    public Description visit(SpecificationFixture fixture) {
        return visit((Fixture) fixture);
    }

    @Override
    public Description visit(ScenarioFixture fixture) {
        return visit((Fixture) fixture);
    }

    @Override
    public Description visit(CaseFixture fixture) {
        return visit((Fixture) fixture);
    }
}
