package com.bitresolution.succor.junit.runners.specs.transform;

import com.bitresolution.succor.junit.runners.specs.model.CaseFixture;
import com.bitresolution.succor.junit.runners.specs.model.Fixture;
import com.bitresolution.succor.junit.runners.specs.model.ScenarioFixture;
import com.bitresolution.succor.junit.runners.specs.model.SpecificationFixture;
import org.junit.runner.notification.RunNotifier;

/**
 *
 */
public interface ExecutionReducer extends FixtureVisitor<Void> {
    ExecutionReducer with(RunNotifier notifier);

    @Override
    Void visit(Fixture fixture);

    @Override
    Void visit(SpecificationFixture fixture);

    @Override
    Void visit(ScenarioFixture fixture);

    @Override
    Void visit(CaseFixture fixture);
}
