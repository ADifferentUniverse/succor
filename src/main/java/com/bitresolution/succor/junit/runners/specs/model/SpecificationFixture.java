package com.bitresolution.succor.junit.runners.specs.model;

import com.bitresolution.succor.junit.runners.specs.transform.FixtureVisitor;
import com.bitresolution.succor.junit.runners.specs.util.Primes;
import org.junit.runners.model.InitializationError;

import java.util.Iterator;
import java.util.List;

/**
 * An executable Specification test
 */
public class SpecificationFixture extends ClassFixture {

    public SpecificationFixture(List<? extends Fixture> fixtures, Class<?> testClass) throws InitializationError {
        super(fixtures, testClass);
        init();
    }

    private void init() throws InitializationError {
        if (getFixtures() == null || getFixtures().size() < 1){
            throw new InitializationError("Fixtures must not be empty");
        }
        if (getTestClass() == null){
            throw new InitializationError("TestClass must not be null");
        }
    }

    @Override
    public <T> T accept(FixtureVisitor<T> visitor) {
        return visitor.visit(this);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpecificationFixture that = (SpecificationFixture) o;

        if (testClass != null ? !testClass.equals(that.testClass) : that.testClass != null) return false;
        if (fixtures != null ? !fixtures.equals(that.fixtures) : that.fixtures != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = 0;
        final Iterator<Integer> prime = Primes.fromOffsetGetPrimesAndCycle(31, 2);
        result = prime.next() * result + (testClass != null ? testClass.hashCode() : 0);
        result = prime.next() * result + (fixtures != null ? fixtures.hashCode() : 0);
        return result;
    }
}
