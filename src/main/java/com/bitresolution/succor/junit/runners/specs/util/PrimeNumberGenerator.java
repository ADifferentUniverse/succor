package com.bitresolution.succor.junit.runners.specs.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 */
public class PrimeNumberGenerator {

    public List<Integer> first(int n) {
        switch (n) {
            case 0:
                return Collections.emptyList();

            case 1:
                return fromZeroTo(2);

            case 2:
                return fromZeroTo(3);

            case 3:
                return fromZeroTo(5);

            case 4:
                return fromZeroTo(7);

            case 5:
                return fromZeroTo(11);

            case 6:
                return fromZeroTo(13);

            default:
                final double estimate = n * (Math.log(n) + Math.log(Math.log(n)));
                final Double bound = Math.ceil(estimate);
                final List<Integer> primes = fromZeroTo(bound.intValue());
                return primes.size() > n ? primes.subList(0, n) : primes;
        }
    }

    public List<Integer> primes(int n, int offset) {
        final List<Integer> integers = first(n + offset);
        return integers.subList(offset, offset + n);
    }

    public List<Integer> fromZeroTo(int n) {
        return fromRange(0, n);
    }

    public List<Integer> fromRange(int n, int m) {
        // initially assume all integers are prime
        boolean[] isPrime = new boolean[m + 1];
        for (int i = 2; i <= m; i++) {
            isPrime[i] = true;
        }

        // mark non-primes <= N using Sieve of Eratosthenes
        for (int i = 2; i*i <= m; i++) {

            // if i is prime, then mark multiples of i as nonprime
            // suffices to consider mutiples i, i+1, ..., N/i
            if (isPrime[i]) {
                for (int j = i; i*j <= m; j++) {
                    isPrime[i*j] = false;
                }
            }
        }

        // count primes
        List<Integer> primes = new ArrayList<Integer>();
        for (int i = n; i <= m; i++) {
            if (isPrime[i]) primes.add(i);
        }
        return primes;
    }
}
