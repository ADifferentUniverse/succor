package com.bitresolution.succor.junit.runners.specs.transform;

import com.bitresolution.succor.junit.runners.specs.model.*;
import org.junit.runner.Description;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunNotifier;
import org.junit.runner.notification.StoppedByUserException;

import java.lang.reflect.Method;

/**
 *
 */
public class DefaultExecutionReducer implements ExecutionReducer {

    private final RunNotifier notifier;
    private final DescriptionReducer descriptor;

    public DefaultExecutionReducer(DescriptionReducer reducer) {
        this(reducer, null);
    }

    private DefaultExecutionReducer(DescriptionReducer reducer, RunNotifier notifier) {
        this.notifier = notifier;
        this.descriptor = reducer;
    }

    @Override
    public ExecutionReducer with(RunNotifier notifier) {
        return new DefaultExecutionReducer(descriptor, notifier);
    }

    @Override
    public Void visit(Fixture fixture) {
        return null;
    }

    @Override
    public Void visit(SpecificationFixture fixture) {
        //run subscenarios if any
        for (Fixture f : fixture.getFixtures()) {
            f.accept(this);
        }
        return null;
    }

    @Override
    public Void visit(ScenarioFixture fixture) {
        if (fixture.getFixtures().isEmpty()) {
                executeFixture(fixture);
        }
        else {
            //run subscenarios if any
            for (Fixture f : fixture.getFixtures()) {
                f.accept(this);
            }
        }
        return null;
    }

    @Override
    public Void visit(CaseFixture fixture) {
        executeFixture(fixture, fixture.getCase());
        return null;
    }

    private void executeFixture(MethodFixture fixture, Object... args) throws StoppedByUserException {
        final Description description = fixture.accept(descriptor);
        try {
            //run this scenario
            notifier.fireTestStarted(description);

            final Method method = fixture.getMethod();
            final Object testInstance = method.getDeclaringClass().newInstance();
            method.invoke(testInstance, args);

            notifier.fireTestFinished(description);
        }
        catch (StoppedByUserException e) {
            throw e;
        }
        catch (Exception e) {
            final Throwable cause = e.getCause() == null ? e : e.getCause();
            notifier.fireTestFailure(new Failure(description, cause));
        }
        catch (Throwable e) {
            notifier.fireTestFailure(new Failure(description, e));
        }
    }
}
