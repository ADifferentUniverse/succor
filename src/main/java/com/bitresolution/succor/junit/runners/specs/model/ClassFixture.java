package com.bitresolution.succor.junit.runners.specs.model;

import java.util.List;

/**
 *
 */
public abstract class ClassFixture extends BaseFixture {

    protected final Class<?> testClass;

    public ClassFixture(List<? extends Fixture> fixtures, Class<?> testClass) {
        super(fixtures);
        this.testClass = testClass;
    }

    public Class<?> getTestClass() {
        return testClass;
    }
}
