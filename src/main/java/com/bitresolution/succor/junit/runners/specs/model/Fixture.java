package com.bitresolution.succor.junit.runners.specs.model;

import com.bitresolution.succor.junit.runners.specs.transform.Visitable;

import java.util.List;

/**
 *
 */
public interface Fixture extends Visitable {
    List<? extends Fixture> getFixtures();
}
