package com.bitresolution.succor.junit.runners.specs;

import com.bitresolution.succor.junit.runners.specs.model.SpecificationFixture;
import org.junit.runners.model.InitializationError;

/**
 * Creates SpecificationFixture, ScenarioFixture, and CaseFixture graph from a given test class.
 */
public interface SpecificationFactory {

    /**
     * Build Specification from class
     * @param testClass the annotated test class
     * @return an executable specification
     */
    SpecificationFixture from(Class<?> testClass) throws InitializationError;
}
