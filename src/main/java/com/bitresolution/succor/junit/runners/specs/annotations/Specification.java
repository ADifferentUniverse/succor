package com.bitresolution.succor.junit.runners.specs.annotations;

import com.bitresolution.succor.junit.runners.specs.SpecificationRunner;
import org.junit.runner.RunWith;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@RunWith(SpecificationRunner.class)
public @interface Specification {
    String name() default "";
}
