package com.bitresolution.succor.junit.runners.specs.transform;

import com.bitresolution.succor.junit.runners.specs.model.CaseFixture;
import com.bitresolution.succor.junit.runners.specs.model.Fixture;
import com.bitresolution.succor.junit.runners.specs.model.ScenarioFixture;
import com.bitresolution.succor.junit.runners.specs.model.SpecificationFixture;

/**
 *
 */
public interface FixtureVisitor<T> {

    T visit(Fixture fixture);
    T visit(SpecificationFixture fixture);
    T visit(ScenarioFixture fixture);
    T visit(CaseFixture fixture);
}
