package com.bitresolution.succor.junit.runners.specs.transform;

/**
 *
 */
public interface Visitable {
    <T> T accept(FixtureVisitor<T> visitor);
}
