package com.bitresolution.succor.junit.runners.specs.model;

import com.bitresolution.succor.junit.runners.specs.transform.FixtureVisitor;
import com.bitresolution.succor.junit.runners.specs.util.Primes;

import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;

/**
 * An executable Scenario test
 */
public class ScenarioFixture extends MethodFixture {

    public ScenarioFixture(List<? extends Fixture> fixtures, Method method) {
        super(fixtures, method);
    }

    @Override
    public <T> T accept(FixtureVisitor<T> visitor) {
        return visitor.visit(this);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ScenarioFixture that = (ScenarioFixture) o;

        if (method != null ? !method.equals(that.method) : that.method != null) return false;
        if (fixtures != null ? !fixtures.equals(that.fixtures) : that.fixtures != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = 0;
        final Iterator<Integer> prime = Primes.fromOffsetGetPrimesAndCycle(31, 2);
        result = prime.next() * result + (method != null ? method.hashCode() : 0);
        result = prime.next() * result + (fixtures != null ? fixtures.hashCode() : 0);
        return result;
    }

}
