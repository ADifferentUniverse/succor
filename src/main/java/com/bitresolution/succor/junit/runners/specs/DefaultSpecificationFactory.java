package com.bitresolution.succor.junit.runners.specs;

import com.bitresolution.succor.junit.runners.specs.annotations.Scenario;
import com.bitresolution.succor.junit.runners.specs.model.CaseFixture;
import com.bitresolution.succor.junit.runners.specs.model.Fixture;
import com.bitresolution.succor.junit.runners.specs.model.ScenarioFixture;
import com.bitresolution.succor.junit.runners.specs.model.SpecificationFixture;
import com.bitresolution.succor.junit.runners.specs.util.AnnotationUtils;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.junit.runners.model.InitializationError;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Default SpecificationBuilder implementation
 */
public class DefaultSpecificationFactory implements SpecificationFactory {

    @Override
    public SpecificationFixture from(Class<?> testClass) throws InitializationError {
        return createSpecification(testClass);
    }

    public SpecificationFixture createSpecification(Class<?> testClass) throws InitializationError {
        return new SpecificationFixture(createScenarios(testClass), testClass);
    }

    public List<Fixture> createScenarios(Class<?> testClass) throws InitializationError {
        List<Fixture> scenarios = new ArrayList<Fixture>();
        Method[] methods = testClass.getMethods();
        for (Method method : methods) {
            final Scenario annotation = (Scenario) AnnotationUtils.getAnnotation(Scenario.class, method);
            if (annotation != null) {
                scenarios.add(createScenarios(method, annotation));
            }
        }
        return scenarios;
    }

    private ScenarioFixture createScenarios(Method method, Scenario annotation) throws InitializationError {
        final int args = method.getParameterTypes().length;
        if (args == 0) {
            return createCaselessScenario(method, annotation);
        }
        if (args == 1) {
            return createScenarioWithCases(method, annotation);
        }
        throw new InitializationError("Scenarios may have 0 or 1 parameters. Found " + args + " parameters on " + method.getName());
    }

    private ScenarioFixture createScenarioWithCases(Method method, Scenario annotation) throws InitializationError {
        final Class<?> parameterType = method.getParameterTypes()[0];
        if (!parameterType.isEnum()) {
            throw new InitializationError("Scenario parameter types must derive from Enum");
        }
        return new ScenarioFixture(generateCases(parameterType, method), method);
    }

    private ScenarioFixture createCaselessScenario(Method method, Scenario annotation) {
        return new ScenarioFixture(Collections.<Fixture>emptyList(), method);
    }


    private List<CaseFixture> generateCases(Class<?> caseClass, final Method method) {
        return Lists.transform(Arrays.asList((Enum[]) caseClass.getEnumConstants()), new Function<Enum, CaseFixture>() {
            @Override
            public CaseFixture apply(Enum scenarioCase) {
                return new CaseFixture(method, scenarioCase);
            }
        });
    }
}
