package examples;

import com.bitresolution.succor.junit.runners.specs.annotations.Scenario;
import com.bitresolution.succor.junit.runners.specs.annotations.Specification;

import static junit.framework.Assert.assertTrue;

@Specification
public class SimpleSpecWithIllegalCaseType {

    @Scenario
    public void shouldBeTrue(ScenarioExample example) {
        assertTrue(true);
    }

    public static class ScenarioExample {
    }
}
