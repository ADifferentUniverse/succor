package examples;

import com.bitresolution.succor.junit.runners.specs.annotations.Scenario;
import com.bitresolution.succor.junit.runners.specs.annotations.Specification;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

@Specification
public class TwoScenarioSpec {

    @Scenario
    public void shouldBeTrue() {
        assertTrue(true);
    }

    @Scenario
    public void shouldBeTrueAndFalse(Cases c) {
        switch (c) {
            case TRUE:
                assertTrue(true);
                break;
            case FALSE:
                assertFalse(false);
                break;
        }
    }

    public static enum Cases {
        TRUE, FALSE
    }
}
