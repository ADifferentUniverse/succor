package examples;

import com.bitresolution.succor.junit.runners.specs.SpecificationRunner;
import com.bitresolution.succor.junit.runners.specs.annotations.Scenario;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertEquals;

/**
 *
 */
//@Specification
@RunWith(SpecificationRunner.class)
public class IntegerComparatorSpec {

    @Scenario
    public void compare(IntegerComparatorCase testCase) {
        assertEquals(testCase.getExpectedValue(), testCase.getLeft().compareTo(testCase.getRight()));
    }

    public static enum IntegerComparatorCase {
        LESS_THAN(1, 2, -1),
        EQUAL(1, 1, 0),
        GREATER_THAN(2, 1, 1);

        private Integer left;
        private Integer right;
        private int expectedValue;

        IntegerComparatorCase(Integer left, Integer right, Integer expectedValue) {
            this.left = left;
            this.right = right;
            this.expectedValue = expectedValue;
        }

        public Integer getLeft() {
            return left;
        }

        public Integer getRight() {
            return right;
        }

        public int getExpectedValue() {
            return expectedValue;
        }
    }
}
