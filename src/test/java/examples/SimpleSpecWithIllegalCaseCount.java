package examples;

import com.bitresolution.succor.junit.runners.specs.annotations.Scenario;
import com.bitresolution.succor.junit.runners.specs.annotations.Specification;

import static junit.framework.Assert.assertTrue;

@Specification
public class SimpleSpecWithIllegalCaseCount {

    @Scenario
    public void shouldBeTrue(ScenarioExampleA exampleA, ScenarioExampleB exampleB) {
        assertTrue(true);
    }

    public static enum ScenarioExampleA {
    }

    public static enum ScenarioExampleB {
    }
}
