package com.bitresolution.succor.spring;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.VerboseMockitoJUnitRunner;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(VerboseMockitoJUnitRunner.class)
public class GracefulShutdownExecutorServiceTest {

    private static final long DURATION = 1L;
    private static final TimeUnit TIME_UNIT = TimeUnit.SECONDS;

    @Mock
    private ExecutorService executor;

    private GracefulShutdownExecutorService service;

    @Before
    public void setUp() throws Exception {
        service = new GracefulShutdownExecutorService(executor, DURATION, TIME_UNIT);
    }

    @Test
    public void shouldGracefullyShutdownDecoratedExecutor() throws Exception {
        //when
        service.shutdown();
        //then
        verify(executor).shutdown();
        verify(executor).awaitTermination(DURATION, TIME_UNIT);
    }

    @Test
    public void shouldThrowExceptionIfTimeoutWhileGracefullyShuttingdownDecoratedExecutor() throws Exception {
        //given
        given(executor.awaitTermination(DURATION, TIME_UNIT)).willThrow(new InterruptedException());
        //when
        Exception exception = null;
        try {
            service.shutdown();
        }
        catch (Exception e) {
            exception = e;
        }
        //then
        assertThat(exception, is(not(nullValue())));
        verify(executor).shutdown();
        verify(executor).awaitTermination(DURATION, TIME_UNIT);
    }

    @Test
    public void shouldProxyToDecoratedExecutorInvocationsOfShutdownNow() throws Exception {
        //when
        service.shutdownNow();
        //then
        verify(executor).shutdownNow();
    }

    @Test
    public void shouldProxyToDecoratedExecutorInvocationsOfIsShutdown() throws Exception {
        //when
        service.isShutdown();
        //then
        verify(executor).isShutdown();
    }

    @Test
    public void shouldProxyToDecoratedExecutorInvocationsOfIsTerminated() throws Exception {
        //when
        service.isTerminated();
        //then
        verify(executor).isTerminated();
    }

    @Test
    public void shouldProxyToDecoratedExecutorInvocationsOfAwaitTermination() throws Exception {
        //when
        service.awaitTermination(DURATION, TIME_UNIT);
        //then
        verify(executor).awaitTermination(DURATION, TIME_UNIT);
    }

    @Test
    public void shouldProxyToDecoratedExecutorInvocationsOfSubmitCallable() throws Exception {
        //given
        Callable callable = mock(Callable.class);
        //when
        service.submit(callable);
        //then
        verify(executor).submit(callable);
    }

    @Test
    public void shouldProxyToDecoratedExecutorInvocationsOfSubmitRunnable() throws Exception {
        //given
        Runnable runnable = mock(Runnable.class);
        //when
        service.submit(runnable);
        //then
        verify(executor).submit(runnable);
    }

    @Test
    public void shouldProxyToDecoratedExecutorInvocationsOfSubmitRunnableWithResult() throws Exception {
        //given
        Runnable runnable = mock(Runnable.class);
        Object result = new Object();
        //when
        service.submit(runnable, result);
        //then
        verify(executor).submit(runnable, result);
    }

    @Test
    public void shouldProxyToDecoratedExecutorInvocationsOfInvokeAll() throws Exception {
        //given
        ArrayList<Callable<Object>> callables = new ArrayList<Callable<Object>>();
        //when
        service.invokeAll(callables);
        //then
        verify(executor).invokeAll(callables);
    }

    @Test
    public void shouldProxyToDecoratedExecutorInvocationsOfInvokeAllWithTimeout() throws Exception {
        //given
        ArrayList<Callable<Object>> callables = new ArrayList<Callable<Object>>();
        //when
        service.invokeAll(callables, DURATION, TIME_UNIT);
        //then
        verify(executor).invokeAll(callables, DURATION, TIME_UNIT);
    }

    @Test
    public void shouldProxyToDecoratedExecutorInvocationsOfInvokeAny() throws Exception {
        //given
        ArrayList<Callable<Object>> callables = new ArrayList<Callable<Object>>();
        //when
        service.invokeAny(callables);
        //then
        verify(executor).invokeAny(callables);
    }

    @Test
    public void shouldProxyToDecoratedExecutorInvocationsOfInvokeAnyWithTimeout() throws Exception {
        //given
        ArrayList<Callable<Object>> callables = new ArrayList<Callable<Object>>();
        //when
        service.invokeAny(callables, DURATION, TIME_UNIT);
        //then
        verify(executor).invokeAny(callables, DURATION, TIME_UNIT);
    }

    @Test
    public void shouldProxyToDecoratedExecutorInvocationsOfExecute() throws Exception {
        //given
        Runnable runnable = mock(Runnable.class);
        //when
        service.execute(runnable);
        //then
        verify(executor).execute(runnable);
    }
}
