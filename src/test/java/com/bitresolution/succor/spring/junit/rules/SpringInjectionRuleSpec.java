package com.bitresolution.succor.spring.junit.rules;

import org.junit.Test;
import org.junit.runner.Description;
import org.junit.runner.RunWith;
import org.junit.runners.model.Statement;
import org.mockito.Mock;
import org.mockito.runners.VerboseMockitoJUnitRunner;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(VerboseMockitoJUnitRunner.class)
public class SpringInjectionRuleSpec {

    @Mock
    private SpringContextFactory factory;
    @Mock
    private ApplicationContext context;
    @Mock
    private Statement statement;
    @Mock
    private Description description;
    @Mock
    private AutowireCapableBeanFactory autowireCapableBeanFactory;

    private Object target = new Object();

    @Test
    public void shouldCreateContextAwareStatement() {
        //given
        given(factory.forTestClass(target.getClass())).willReturn(context);
        SpringInjectionRule rule = new SpringInjectionRule(target, factory);
        //when
        Statement processedStatement = rule.apply(statement, description);
        //then
        assertThat(processedStatement, is(instanceOf(SpringInjectionRule.WiredStatement.class)));
    }

    @Test
    public void shouldAutowireTargetBeforeExecutingStatement() throws Throwable {
        //given
        given(factory.forTestClass(target.getClass())).willReturn(context);
        given(context.getAutowireCapableBeanFactory()).willReturn(autowireCapableBeanFactory);

        SpringInjectionRule rule = new SpringInjectionRule(target, factory);
        Statement processedStatement = rule.apply(statement, description);
        //when
        processedStatement.evaluate();
        //then
        verify(context).getAutowireCapableBeanFactory();
        verify(autowireCapableBeanFactory).autowireBean(target);
        verify(statement).evaluate();
    }
}
