package com.bitresolution.succor.collections

import spock.lang.Specification


class IterableFunctionSpec extends Specification {

    def "should interleave items"() {
        expect:
        result == IterableFunction.interleave(left, right).toList()

        where:
        left             | right           | result
        []               | []              | []
        ["1"]            | []              | ["1"]
        []               | ["a"]           | ["a"]
        ["1", "2"]       | []              | ["1", "2"]
        []               | ["a", "b"]      | ["a", "b"]
        ["1", "2", "3"]  | ["a", "b"]      | ["1", "a", "2", "b", "3"]
        ["1", "2"]       | ["a", "b", "c"] | ["1", "a", "2", "b", "c"]
    }
}
