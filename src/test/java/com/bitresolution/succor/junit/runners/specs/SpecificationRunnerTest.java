package com.bitresolution.succor.junit.runners.specs;

import com.bitresolution.succor.junit.runners.specs.model.SpecificationFixture;
import com.bitresolution.succor.junit.runners.specs.transform.DescriptionReducer;
import com.bitresolution.succor.junit.runners.specs.transform.ExecutionReducer;
import examples.EmptySpec;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.model.InitializationError;
import org.mockito.Mock;
import org.mockito.runners.VerboseMockitoJUnitRunner;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(VerboseMockitoJUnitRunner.class)
public class SpecificationRunnerTest {

    private static final Class<EmptySpec> TEST_CLASS = EmptySpec.class;

    @Mock
    private SpecificationFactory factory;

    @Mock
    private DescriptionReducer descriptor;

    @Mock
    public ExecutionReducer executor;

    @Mock
    private SpecificationFixture specification;

    private SpecificationRunner runner;

    @Before
    public void setup() throws InitializationError {
        given(factory.from(TEST_CLASS)).willReturn(specification);
        runner = new SpecificationRunner(TEST_CLASS, factory, descriptor, executor);
    }
    
    @Test
    public void testGetDescription() throws Exception {
        //given
        //when
        runner.getDescription();

        //then
        verify(descriptor).visit(specification);
    }

    @Test
    public void testRun() throws Exception {
        //given
        RunNotifier notifier = mock(RunNotifier.class);

        given(executor.with(any(RunNotifier.class))).willReturn(executor);

        //when
        runner.run(notifier);

        //then
        verify(executor).visit(specification);
    }
}
