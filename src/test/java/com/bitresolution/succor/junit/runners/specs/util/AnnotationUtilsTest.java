package com.bitresolution.succor.junit.runners.specs.util;

import examples.annotationutils.*;
import org.junit.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import static junit.framework.Assert.*;

/**
 *
 */
public class AnnotationUtilsTest {

    @Test
    public void shouldNotFindExtendedAnnotationOnClassIfNotDeclared() {
        final Annotation annotation = this.getClass().getAnnotation(Base.class);
        assertNull(annotation);
    }

    @Test
    public void shouldNotFindExtendedAnnotationOnClass() {
        final Annotation annotation = ClassLevelOnlyAnnotations.class.getAnnotation(Base.class);
        assertNull(annotation);
    }

    @Test
    public void shouldFindExtendedAnnotationOnClass() {
        final Annotation annotation = AnnotationUtils.getAnnotation(Base.class, ClassLevelOnlyAnnotations.class);
        assertNotNull(annotation);
    }

    @Test
    public void shouldNotFindExtendedAnnotationOnClassWhenOnMethodAlso() {
        final Annotation annotation = ClassLevelAndMethodAnnotations.class.getAnnotation(Base.class);
        assertNull(annotation);
    }

    @Test
    public void shouldNotFindExtendedAnnotationOnClassWhenOnMethodOnly() {
        final Annotation annotation = AnnotationUtils.getAnnotation(Base.class, MethodOnlyAnnotations.class);
        assertNull(annotation);
    }

    @Test
    public void shouldNotFindExtendedAnnotationOnMethod() {
        final Annotation annotation = MethodOnlyAnnotations.class.getMethods()[0].getAnnotation(Base.class);
        assertNull(annotation);
    }

    @Test
    public void shouldNotFindExtendedAnnotationOnMethodIfNotPresent() {
        final Annotation annotation = AnnotationUtils.getAnnotation(Base.class, NoAnnotations.class.getMethods()[0]);
        assertNull(annotation);
    }

    @Test
    public void shouldFindExtendedAnnotationOnMethod() {
        final Annotation annotation = AnnotationUtils.getAnnotation(Base.class, MethodOnlyAnnotations.class.getMethods()[0]);
        assertNotNull(annotation);
    }

    @Test(expected = IllegalStateException.class)
    public void shouldHavePrivateConstructor() throws Throwable {
        Constructor[] constructors = AnnotationUtils.class.getDeclaredConstructors();

        assertTrue(constructors.length == 1);
        assertFalse(constructors[0].isAccessible());

        //just for the hell of it
        constructors[0].setAccessible(true);// Change the accessible property of the constructor.
        try {
            constructors[0].newInstance(null);
        }
        catch (InvocationTargetException e) {
            throw e.getCause();
        }
    }
}
