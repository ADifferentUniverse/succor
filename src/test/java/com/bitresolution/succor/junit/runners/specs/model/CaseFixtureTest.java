package com.bitresolution.succor.junit.runners.specs.model;

import com.bitresolution.succor.junit.runners.specs.transform.FixtureVisitor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.Method;
import java.util.Collections;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Method.class)
public class CaseFixtureTest {

    private static enum Case {
        NONE,
        SOME
        ;
    }

    private static class TwoMethodClass {
        public void methodA(){}
        public void methodB(){}
    }

    private Method method;

    @Before
    public void setUp() throws Exception {
        method = mock(Method.class);
    }

    @Test
    public void shouldAcceptFixtureVisitor() {
        //given
        CaseFixture fixture = new CaseFixture(method, Case.NONE);
        final CaseFixture mockFixture = spy(fixture);

        final FixtureVisitor visitor = Mockito.mock(FixtureVisitor.class);

        //when
        mockFixture.accept(visitor);

        //then
        verify(visitor).visit(mockFixture);
    }

    @Test
    public void equalsContract() {
        final Method[] methods = TwoMethodClass.class.getDeclaredMethods();
        Method methodA = methods[0];
        Method methodB = methods[1];
        method = methods[0]; //reassigning test variable!


        final CaseFixture noneAOne = new CaseFixture(methodA, Case.NONE);
        final CaseFixture noneATwo = new CaseFixture(methodA, Case.NONE);

        final CaseFixture someAOne = new CaseFixture(methodA, Case.SOME);
        final CaseFixture someATwo = new CaseFixture(methodA, Case.SOME);

        final CaseFixture noneBOne = new CaseFixture(methodB, Case.NONE);
        final CaseFixture noneBTwo = new CaseFixture(methodB, Case.NONE);

        final CaseFixture someBOne = new CaseFixture(methodB, Case.SOME);
        final CaseFixture someBTwo = new CaseFixture(methodB, Case.SOME);

        final CaseFixture noneNullOne = new CaseFixture(null, Case.NONE);
        final CaseFixture noneNullTwo = new CaseFixture(null, Case.NONE);

        final CaseFixture nullMethodOne = new CaseFixture(method, null);
        final CaseFixture nullMethodTwo = new CaseFixture(method, null);

        assertFalse(someAOne.equals(null));
        assertFalse(someAOne.equals(Collections.emptyList()));

        assertTrue(noneAOne.equals(noneATwo));
        assertTrue(noneAOne.equals(noneAOne));

        assertTrue(noneBOne.equals(noneBTwo));
        assertTrue(someAOne.equals(someATwo));
        assertTrue(someBOne.equals(someBTwo));
        assertTrue(noneNullOne.equals(noneNullTwo));
        assertTrue(nullMethodOne.equals(nullMethodTwo));

        assertFalse(noneAOne.equals(noneBOne));
        assertFalse(noneAOne.equals(someAOne));
        assertFalse(noneAOne.equals(someBOne));
        assertFalse(noneAOne.equals(noneNullOne));
        assertFalse(noneAOne.equals(nullMethodOne));

        assertFalse(noneBOne.equals(noneAOne));
        assertFalse(noneBOne.equals(someAOne));
        assertFalse(noneBOne.equals(someBOne));
        assertFalse(noneBOne.equals(noneNullOne));
        assertFalse(noneBOne.equals(nullMethodOne));

        assertFalse(someAOne.equals(noneAOne));
        assertFalse(someAOne.equals(noneBOne));
        assertFalse(someAOne.equals(someBOne));
        assertFalse(someAOne.equals(noneNullOne));
        assertFalse(someAOne.equals(nullMethodOne));

        assertFalse(someBOne.equals(noneAOne));
        assertFalse(someBOne.equals(noneBOne));
        assertFalse(someBOne.equals(someAOne));
        assertFalse(someBOne.equals(noneNullOne));
        assertFalse(someBOne.equals(nullMethodOne));

        assertFalse(noneNullOne.equals(noneAOne));
        assertFalse(noneNullOne.equals(noneBOne));;
        assertFalse(noneNullOne.equals(someAOne));
        assertFalse(noneNullOne.equals(someBOne));
        assertFalse(noneNullOne.equals(nullMethodOne));

        assertFalse(nullMethodOne.equals(noneAOne));
        assertFalse(nullMethodOne.equals(noneBOne));
        assertFalse(nullMethodOne.equals(someAOne));
        assertFalse(nullMethodOne.equals(someBOne));
        assertFalse(nullMethodOne.equals(noneNullOne));
    }

    @Test
    public void hashCodeContract() {
        final Method[] methods = TwoMethodClass.class.getDeclaredMethods();
        Method methodA = methods[0];
        Method methodB = methods[1];
        method = methods[0]; //reassigning test variable!


        final CaseFixture noneAOne = new CaseFixture(methodA, Case.NONE);
        final CaseFixture noneATwo = new CaseFixture(methodA, Case.NONE);

        final CaseFixture someAOne = new CaseFixture(methodA, Case.SOME);
        final CaseFixture someATwo = new CaseFixture(methodA, Case.SOME);

        final CaseFixture noneBOne = new CaseFixture(methodB, Case.NONE);
        final CaseFixture noneBTwo = new CaseFixture(methodB, Case.NONE);

        final CaseFixture someBOne = new CaseFixture(methodB, Case.SOME);
        final CaseFixture someBTwo = new CaseFixture(methodB, Case.SOME);

        final CaseFixture noneNullOne = new CaseFixture(null, Case.NONE);
        final CaseFixture noneNullTwo = new CaseFixture(null, Case.NONE);

        final CaseFixture nullMethodOne = new CaseFixture(method, null);
        final CaseFixture nullMethodTwo = new CaseFixture(method, null);

        assertTrue(noneAOne.hashCode() == noneATwo.hashCode());
        assertTrue(noneAOne.hashCode() == noneAOne.hashCode());

        assertTrue(noneBOne.hashCode() == noneBTwo.hashCode());
        assertTrue(someAOne.hashCode() == someATwo.hashCode());
        assertTrue(someBOne.hashCode() == someBTwo.hashCode());
        assertTrue(noneNullOne.hashCode() == noneNullTwo.hashCode());
        assertTrue(nullMethodOne.hashCode() == nullMethodTwo.hashCode());

        assertFalse(noneAOne.hashCode() == noneBOne.hashCode());
        assertFalse(noneAOne.hashCode() == someAOne.hashCode());
        assertFalse(noneAOne.hashCode() == someBOne.hashCode());
        assertFalse(noneAOne.hashCode() == noneNullOne.hashCode());
        assertFalse(noneAOne.hashCode() == nullMethodOne.hashCode());

        assertFalse(noneBOne.hashCode() == noneAOne.hashCode());
        assertFalse(noneBOne.hashCode() == someAOne.hashCode());
        assertFalse(noneBOne.hashCode() == someBOne.hashCode());
        assertFalse(noneBOne.hashCode() == noneNullOne.hashCode());
        assertFalse(noneBOne.hashCode() == nullMethodOne.hashCode());

        assertFalse(someAOne.hashCode() == noneAOne.hashCode());
        assertFalse(someAOne.hashCode() == noneBOne.hashCode());
        assertFalse(someAOne.hashCode() == someBOne.hashCode());
        assertFalse(someAOne.hashCode() == noneNullOne.hashCode());
        assertFalse(someAOne.hashCode() == nullMethodOne.hashCode());

        assertFalse(someBOne.hashCode() == noneAOne.hashCode());
        assertFalse(someBOne.hashCode() == noneBOne.hashCode());
        assertFalse(someBOne.hashCode() == someAOne.hashCode());
        assertFalse(someBOne.hashCode() == noneNullOne.hashCode());
        assertFalse(someBOne.hashCode() == nullMethodOne.hashCode());

        assertFalse(noneNullOne.hashCode() == noneAOne.hashCode());
        assertFalse(noneNullOne.hashCode() == noneBOne.hashCode());;
        assertFalse(noneNullOne.hashCode() == someAOne.hashCode());
        assertFalse(noneNullOne.hashCode() == someBOne.hashCode());
        assertFalse(noneNullOne.hashCode() == nullMethodOne.hashCode());

        assertFalse(nullMethodOne.hashCode() == noneAOne.hashCode());
        assertFalse(nullMethodOne.hashCode() == noneBOne.hashCode());
        assertFalse(nullMethodOne.hashCode() == someAOne.hashCode());
        assertFalse(nullMethodOne.hashCode() == someBOne.hashCode());
        assertFalse(nullMethodOne.hashCode() == noneNullOne.hashCode());
    }
}
