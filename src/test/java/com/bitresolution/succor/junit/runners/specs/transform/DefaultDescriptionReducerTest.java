package com.bitresolution.succor.junit.runners.specs.transform;

import com.bitresolution.succor.junit.runners.specs.MockFixture;
import com.bitresolution.succor.junit.runners.specs.model.CaseFixture;
import com.bitresolution.succor.junit.runners.specs.model.Fixture;
import com.bitresolution.succor.junit.runners.specs.model.ScenarioFixture;
import com.bitresolution.succor.junit.runners.specs.model.SpecificationFixture;
import examples.IntegerComparatorSpec;
import examples.TwoScenarioSpec;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.Description;
import org.junit.runners.model.InitializationError;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;

import static junit.framework.Assert.assertEquals;

public class DefaultDescriptionReducerTest {

    private static final Class<?> TEST_CLASS = IntegerComparatorSpec.class;

    private DescriptionReducer reducer;

    @Before
    public void setup() {
        reducer = new DefaultDescriptionReducer();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void shouldThrowExceptionForGenericTypeFixtures() {
        //given
        Fixture fixture = new MockFixture();

        //when
        fixture.accept(reducer);

        //then exception
    }

    @Test
    public void shouldDescribeSpecWithOneScenario() throws InitializationError {
        //given
        final Method method = TEST_CLASS.getDeclaredMethods()[0];
        final SpecificationFixture specificationFixture = new SpecificationFixture(
                Arrays.asList(new ScenarioFixture(Collections.<Fixture>emptyList(), method)),
                TEST_CLASS
        );

        //when
        final Description actual = specificationFixture.accept(reducer);

        //then
        final Description expected = Description.createSuiteDescription(TEST_CLASS);
        expected.addChild(Description.createTestDescription(TEST_CLASS, method.getName(), method.getAnnotations()));

        assertEquals(expected, actual);
    }

    @Test
    public void shouldDescribeSpecWithOneCompoundScenario() throws InitializationError {
        //given
        final Method method = TEST_CLASS.getDeclaredMethods()[0];
        final SpecificationFixture specificationFixture = new SpecificationFixture(
                Arrays.asList(new ScenarioFixture(
                        Arrays.asList(
                                new CaseFixture(method, IntegerComparatorSpec.IntegerComparatorCase.LESS_THAN),
                                new CaseFixture(method, IntegerComparatorSpec.IntegerComparatorCase.EQUAL),
                                new CaseFixture(method, IntegerComparatorSpec.IntegerComparatorCase.GREATER_THAN)
                        ),
                        method)),
                TEST_CLASS
        );

        //when
        final Description actual = specificationFixture.accept(reducer);

        //then
        final Description expected = Description.createSuiteDescription(TEST_CLASS);
        final Description scenario = Description.createSuiteDescription(method.getName(), method.getAnnotations());
        scenario.addChild(Description.createTestDescription(TEST_CLASS, IntegerComparatorSpec.IntegerComparatorCase.LESS_THAN.name(), method.getAnnotations()));
        scenario.addChild(Description.createTestDescription(TEST_CLASS, IntegerComparatorSpec.IntegerComparatorCase.EQUAL.name(), method.getAnnotations()));
        scenario.addChild(Description.createTestDescription(TEST_CLASS, IntegerComparatorSpec.IntegerComparatorCase.GREATER_THAN.name(), method.getAnnotations()));
        expected.addChild(scenario);

        assertEquals(expected, actual);
    }

    @Test
    public void shouldDescribeSpecWithScenarios() throws InitializationError {
        //given
        final Method[] method = TwoScenarioSpec.class.getDeclaredMethods();
        final SpecificationFixture specificationFixture = new SpecificationFixture(
                Arrays.asList(
                        new ScenarioFixture(Collections.<Fixture>emptyList(), method[0]),
                        new ScenarioFixture(
                                Arrays.asList(
                                        new CaseFixture(method[1], TwoScenarioSpec.Cases.TRUE),
                                        new CaseFixture(method[1], TwoScenarioSpec.Cases.FALSE)
                                ),
                                method[1])),
                TwoScenarioSpec.class
        );

        //when
        final Description actual = specificationFixture.accept(reducer);

        //then
        final Description expected = Description.createSuiteDescription(TwoScenarioSpec.class);
        expected.addChild(Description.createTestDescription(TEST_CLASS, method[0].getName(), method[0].getAnnotations()));

        final Description scenario = Description.createSuiteDescription(method[1].getName(), method[1].getAnnotations());
        scenario.addChild(Description.createTestDescription(TwoScenarioSpec.class, TwoScenarioSpec.Cases.TRUE.name(), method[1].getAnnotations()));
        scenario.addChild(Description.createTestDescription(TwoScenarioSpec.class, TwoScenarioSpec.Cases.FALSE.name(), method[1].getAnnotations()));
        expected.addChild(scenario);

        assertEquals(expected, actual);
    }
}
