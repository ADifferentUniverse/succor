package com.bitresolution.succor.junit.runners.specs;

import com.bitresolution.succor.junit.runners.specs.model.CaseFixture;
import com.bitresolution.succor.junit.runners.specs.model.Fixture;
import com.bitresolution.succor.junit.runners.specs.model.ScenarioFixture;
import com.bitresolution.succor.junit.runners.specs.model.SpecificationFixture;
import examples.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runners.model.InitializationError;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static junit.framework.Assert.assertEquals;

/**
 *
 */
public class DefaultSpecificationFactoryTest {

    private static enum Case {
        NONE;
    }

    private DefaultSpecificationFactory factory;

    @Before
    public void setUp() throws Exception {
        factory = new DefaultSpecificationFactory();
    }

    @Test(expected = InitializationError.class)
    public void shouldNotConstructEmptySpecification() throws InitializationError {
        //when
        factory.from(EmptySpec.class);
    }

    @Test
    public void shouldConstructSpecificationWithCaselessScenarios() throws InitializationError {
        //when
        final Class<SimpleSpec> testClass = SimpleSpec.class;
        final SpecificationFixture fixture = factory.from(testClass);

        //then
        List<Fixture> fixtures = new ArrayList<Fixture>();
        for (Method method : testClass.getDeclaredMethods()) {
            fixtures.add(new ScenarioFixture(Collections.<Fixture>emptyList(), method));
        }
        final SpecificationFixture expected = new SpecificationFixture(fixtures, testClass);

        assertEquals(expected, fixture);
    }

    @Test
    public void          shouldConstructSpecificationWithCasedScenarios() throws InitializationError {
        //when
        final Class<SimpleSpecWithExamples> testClass = SimpleSpecWithExamples.class;
        final SpecificationFixture fixture = factory.from(testClass);

        //then
        Method method = testClass.getDeclaredMethods()[0];
        final SpecificationFixture expected = new SpecificationFixture(
                Arrays.asList(
                        new ScenarioFixture(
                                Arrays.asList(
                                    new CaseFixture(method, SimpleSpecWithExamples.ScenarioExample.CASE_A),
                                    new CaseFixture(method, SimpleSpecWithExamples.ScenarioExample.CASE_B)
                                ),
                                method)
                ),
                testClass);

        assertEquals(expected, fixture);
    }

    @Test(expected = InitializationError.class)
    public void shouldNotConstructSpecificationWhenScenarioCaseIsNotAnEnum() throws InitializationError {
        //when
        final Class<SimpleSpecWithIllegalCaseType> testClass = SimpleSpecWithIllegalCaseType.class;
        final SpecificationFixture fixture = factory.from(testClass);
    }

    @Test(expected = InitializationError.class)
    public void shouldNotConstructSpecificationWhenScenarioHasMoreThanOneCaseArgument() throws InitializationError {
        //when
        final Class<SimpleSpecWithIllegalCaseCount> testClass = SimpleSpecWithIllegalCaseCount.class;
        final SpecificationFixture fixture = factory.from(testClass);
    }
}
