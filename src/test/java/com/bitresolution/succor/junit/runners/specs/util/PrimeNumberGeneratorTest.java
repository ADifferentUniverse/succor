package com.bitresolution.succor.junit.runners.specs.util;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 *
 */
public class PrimeNumberGeneratorTest {

    /**
     * Taken from Wikipedia
     */
    public static final Integer[] PRIMES = {
            2, 3, 5, 7, 11, 13, 17, 19, 23, 29,
            31, 37, 41, 43, 47, 53, 59, 61, 67, 71,
            73, 79, 83, 89, 97, 101, 103, 107, 109, 113,
            127, 131, 137, 139, 149, 151, 157, 163, 167, 173,
            179, 181, 191, 193, 197, 199, 211, 223, 227, 229,
            233, 239, 241, 251, 257, 263, 269, 271, 277, 281,
            283, 293, 307, 311, 313, 317, 331, 337, 347, 349,
            353, 359, 367, 373, 379, 383, 389, 397, 401, 409,
            419, 421, 431, 433, 439, 443, 449, 457, 461, 463,
            467, 479, 487, 491, 499, 503, 509, 521, 523, 541,
            547, 557, 563, 569, 571, 577, 587, 593, 599, 601,
            607, 613, 617, 619, 631, 641, 643, 647, 653, 659,
            661, 673, 677, 683, 691, 701, 709, 719, 727, 733,
            739, 743, 751, 757, 761, 769, 773, 787, 797, 809,
            811, 821, 823, 827, 829, 839, 853, 857, 859, 863,
            877, 881, 883, 887, 907, 911, 919, 929, 937, 941,
            947, 953, 967, 971, 977, 983, 991, 997
    };

    private PrimeNumberGenerator generator;

    @Before
    public void setUp() throws Exception {
        generator = new PrimeNumberGenerator();
    }

    @Test
    public void shouldReturnEmptyListIfNoPrimesInRange() {
        //given
        int n = 1;
        //when
        final List<Integer> primes = generator.fromZeroTo(n);
        //then
        assertTrue(primes.isEmpty());
    }

    @Test
    public void shouldReturnOnePrimeFromDefaultRange() {
        //given
        int n = 2;
        //when
        final List<Integer> primes = generator.fromZeroTo(n);
        //then
        assertEquals(1, primes.size());
        for (int i = 0; i < primes.size(); i++) {
            assertEquals(PRIMES[i], primes.get(i));
        }
    }

    @Test
    public void shouldReturnTwoPrimesFromDefaultRange() {
        //given
        int n = 3;
        //when
        final List<Integer> primes = generator.fromZeroTo(n);
        //then
        assertEquals(2, primes.size());
        for (int i = 0; i < primes.size(); i++) {
            assertEquals(PRIMES[i], primes.get(i));
        }
    }

    @Test
    public void shouldReturnPrimesFromRange() {
        //given
        int n = 3;
        int m = 10;

        //when
        final List<Integer> primes = generator.fromRange(n, m);
        //then
        assertEquals(3, primes.size());
        for (int i = n; i < primes.size(); i++) {
            assertEquals(PRIMES[i], primes.get(i));
        }
    }

    @Test
    public void shouldReturnEmptyListForNoPrimes() {
        //given
        int n = 0;
        //when
        final List<Integer> primes = generator.first(n);
        //then
        assertEquals(n, primes.size());
    }

    @Test
    public void shouldGenerateOnePrime() {
        //given
        int n = 1;
        //when
        final List<Integer> primes = generator.first(n);
        //then
        assertEquals(n, primes.size());
        for (int i = 0; i < n; i++) {
            assertEquals(PRIMES[i], primes.get(i));
        }
    }

    @Test
    public void shouldGenerateTwoPrimes() {
        //given
        int n = 2;
        //when
        final List<Integer> primes = generator.first(n);
        //then
        assertEquals(n, primes.size());
        for (int i = 0; i < n; i++) {
            assertEquals(PRIMES[i], primes.get(i));
        }
    }

    @Test
    public void shouldGenerateXPrimes() {
        //given
        int x = PRIMES.length;
        for (int n = 0; n < x; n++) {
            //when
            final List<Integer> primes = generator.first(n);
            //then
            assertEquals(n, primes.size());
            for (int i = 0; i < n; i++) {
                assertEquals("For x = " + n, PRIMES[i], primes.get(i));
            }
        }
    }

    @Test
    public void shouldReturnEmptyListForNoPrimesWithOffset() {
        //given
        int n = 0;
        int offset = 2;
        //when
        final List<Integer> primes = generator.primes(n, offset);
        //then
        assertEquals(n, primes.size());
    }

    @Test
    public void shouldGenerateOnePrimeWithOffset() {
        //given
        int n = 1;
        int offset = 2;
        //when
        final List<Integer> primes = generator.primes(n, offset);
        //then
        assertEquals(n, primes.size());
        for (int i = offset; i < n; i++) {
            assertEquals(PRIMES[i], primes.get(i));
        }
    }

    @Test
    public void shouldGenerateTwoPrimesWithOffset() {
        //given
        int n = 2;
        int offset = 2;
        //when
        final List<Integer> primes = generator.primes(n, offset);
        //then
        assertEquals(n, primes.size());
        for (int i = offset; i < n; i++) {
            assertEquals(PRIMES[i], primes.get(i));
        }
    }

    @Test
    public void shouldGenerateXPrimesWithOffset() {
        //given
        int x = PRIMES.length;
        int offset = 2;
        //when
        for (int n = offset; n < x; n++) {
            //when
            final List<Integer> primes = generator.first(n);
            //then
            assertEquals(n, primes.size());
            for (int i = 0; i < n; i++) {
                assertEquals("For x = " + n, PRIMES[i], primes.get(i));
            }
        }
    }
}
