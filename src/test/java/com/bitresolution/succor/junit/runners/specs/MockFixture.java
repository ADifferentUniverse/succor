package com.bitresolution.succor.junit.runners.specs;

import com.bitresolution.succor.junit.runners.specs.model.Fixture;
import com.bitresolution.succor.junit.runners.specs.transform.FixtureVisitor;

import java.lang.annotation.Annotation;
import java.util.List;

/**
 *
 */
public class MockFixture implements Fixture {

    private String name;
    private Annotation[] annotations;
    private List<? extends Fixture> fixtures;
    private Class<?> testClass;

    @Override
    public <T> T accept(FixtureVisitor<T> visitor) {
        return visitor.visit(this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Annotation[] getAnnotations() {
        return annotations;
    }

    public void setAnnotations(Annotation[] annotations) {
        this.annotations = annotations;
    }

    public List<? extends Fixture> getFixtures() {
        return fixtures;
    }

    public void setFixtures(List<? extends Fixture> fixtures) {
        this.fixtures = fixtures;
    }

    public Class<?> getTestClass() {
        return testClass;
    }

    public void setTestClass(Class<?> testClass) {
        this.testClass = testClass;
    }
}
