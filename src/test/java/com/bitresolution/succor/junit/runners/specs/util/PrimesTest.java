package com.bitresolution.succor.junit.runners.specs.util;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static junit.framework.Assert.*;

/**
 *
 */
public class PrimesTest {

    @Test
    public void shouldGenerateIteratorWithFirstPrime() {
        //given
        //when
        final int count = 1;
        final int offset = 0;
        final Iterator<Integer> iterator = Primes.fromOffsetGetPrimesAndCycle(offset, count);

        //then
        List<Integer> primes = new ArrayList<Integer>();
        while (iterator.hasNext()) {
            primes.add(iterator.next());
            iterator.remove();
        }

        assertEquals(count, primes.size());
        assertEquals(2, primes.get(0).intValue());
    }

    @Test
    public void shouldGenerateIteratorWithFirstTwoPrime() {
        //given
        //when
        final int count = 2;
        final int offset = 0;
        final Iterator<Integer> iterator = Primes.fromOffsetGetPrimesAndCycle(offset, count);

        //then
        List<Integer> primes = new ArrayList<Integer>();
        while (iterator.hasNext()) {
            primes.add(iterator.next());
            iterator.remove();
        }

        assertEquals(count, primes.size());
        assertEquals(2, primes.get(0).intValue());
        assertEquals(3, primes.get(1).intValue());
    }

    @Test
    public void shouldGenerateIteratorWithFirstTwoPrimesAfterOffset() {
        //given
        //when
        final int count = 2;
        final int offset = 3;
        final Iterator<Integer> iterator = Primes.fromOffsetGetPrimesAndCycle(offset, count);

        //then
        List<Integer> primes = new ArrayList<Integer>();
        while (iterator.hasNext()) {
            primes.add(iterator.next());
            iterator.remove();
        }

        assertEquals(count, primes.size());
        assertEquals(7, primes.get(0).intValue());
        assertEquals(11, primes.get(1).intValue());
    }

    @Test
    public void shouldGenerateCyclicIteratorWithFirstTwoPrimesAfterOffset() {
        //given
        //when
        final int count = 2;
        final int offset = 3;
        final Iterator<Integer> iterator = Primes.fromOffsetGetPrimesAndCycle(offset, count);

        //then
        for (int i = 0; i < 5 * count; i++) { //far more loops than there should be primes generated
            assertTrue(iterator.hasNext());
            assertNotNull(iterator.next());
        }
    }


    @Test(expected = IllegalStateException.class)
    public void shouldHavePrivateConstructor() throws Throwable {
        Constructor[] constructors = Primes.class.getDeclaredConstructors();

        assertTrue(constructors.length == 1);
        assertFalse(constructors[0].isAccessible());

        //just for the hell of it
        constructors[0].setAccessible(true);// Change the accessible property of the constructor.
        try {
            constructors[0].newInstance(null);
        }
        catch (InvocationTargetException e) {
            throw e.getCause();
        }
    }

}
