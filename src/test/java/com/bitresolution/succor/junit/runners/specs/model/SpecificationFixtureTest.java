package com.bitresolution.succor.junit.runners.specs.model;

import com.bitresolution.succor.junit.runners.specs.transform.FixtureVisitor;
import examples.EmptySpec;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.model.InitializationError;
import org.mockito.runners.VerboseMockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(VerboseMockitoJUnitRunner.class)
public class SpecificationFixtureTest {


    private static final Class<EmptySpec> TEST_CLASS = EmptySpec.class;
    private static final List<? extends Fixture> EMPTY_FIXTURES = Collections.emptyList();
    private static final List<? extends Fixture> NON_EMPTY_FIXTURES = Arrays.asList(mock(Fixture.class));

    @Test(expected = InitializationError.class)
    public void shouldThrowExceptionIfFixturesIsNull() throws InitializationError {
        new SpecificationFixture(null, TEST_CLASS);
    }

    @Test(expected = InitializationError.class)
    public void shouldThrowExceptionIfFixturesIsEmpty() throws InitializationError {
        new SpecificationFixture(EMPTY_FIXTURES, TEST_CLASS);
    }

    @Test(expected = InitializationError.class)
    public void shouldThrowExceptionIfTestClassIsNull() throws InitializationError {
        new SpecificationFixture(NON_EMPTY_FIXTURES, null);
    }

    @Test
    public void shouldAcceptFixtureVisitor() throws InitializationError {
        //given
        final SpecificationFixture fixture = new SpecificationFixture(NON_EMPTY_FIXTURES, TEST_CLASS);
        final SpecificationFixture mockFixture = spy(fixture);

        final FixtureVisitor visitor = mock(FixtureVisitor.class);

        //when
        mockFixture.accept(visitor);

        //then
        verify(visitor).visit(mockFixture);
    }

    @Test
    public void equalsContract() {
        EqualsVerifier.forClass(SpecificationFixture.class)
                .usingGetClass()
                .suppress(Warning.STRICT_INHERITANCE)
                .verify();

    }
}
