package com.bitresolution.succor.junit.runners.specs.model;

import com.bitresolution.succor.junit.runners.specs.MockFixture;
import com.bitresolution.succor.junit.runners.specs.transform.FixtureVisitor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Method.class)
public class ScenarioFixtureTest {

    private static class TwoMethodClass {
        public void methodA(){}
        public void methodB(){}
    }

    private static final List<? extends Fixture> EMPTY_FIXTURES = Collections.emptyList();
    private static final List<? extends Fixture> ONE_FIXTURE = Arrays.asList(new MockFixture());

    private Method method;

    @Before
    public void setUp() throws Exception {
        method = PowerMockito.mock(Method.class);
    }

    @Test
    public void shouldAcceptFixtureVisitor() {
        //given
        ScenarioFixture fixture = new ScenarioFixture(EMPTY_FIXTURES, method);
        final ScenarioFixture mockFixture = spy(fixture);

        final FixtureVisitor visitor = mock(FixtureVisitor.class);

        //when
        mockFixture.accept(visitor);

        //then
        verify(visitor).visit(mockFixture);
    }


    @Test
    public void equalsContract() {
        final Method[] methods = TwoMethodClass.class.getDeclaredMethods();
        Method methodA = methods[0];
        Method methodB = methods[1];
        method = methods[0]; //reassigning test variable!

        final ScenarioFixture emptyA = new ScenarioFixture(EMPTY_FIXTURES, methodA);
        final ScenarioFixture emptyAcopy = new ScenarioFixture(EMPTY_FIXTURES, methodA);
        final ScenarioFixture emptyB = new ScenarioFixture(EMPTY_FIXTURES, methodB);
        final ScenarioFixture emptyNull = new ScenarioFixture(EMPTY_FIXTURES, null);

         final ScenarioFixture oneA = new ScenarioFixture(ONE_FIXTURE, methodA);
        final ScenarioFixture oneAcopy = new ScenarioFixture(ONE_FIXTURE, methodA);
        final ScenarioFixture oneB = new ScenarioFixture(ONE_FIXTURE, methodB);
        final ScenarioFixture oneNull = new ScenarioFixture(ONE_FIXTURE, null);

        final ScenarioFixture nullA = new ScenarioFixture(null, methodA);
        final ScenarioFixture nullAcopy = new ScenarioFixture(null, methodA);
        final ScenarioFixture nullB = new ScenarioFixture(null, methodB);
        final ScenarioFixture nullNull = new ScenarioFixture(null, null);
        final ScenarioFixture nullNullcopy = new ScenarioFixture(null, null);

        assertTrue(emptyA.equals(emptyA));
        assertFalse(emptyA.equals(Collections.emptyList()));

        assertTrue(emptyA.equals(emptyAcopy));
        assertFalse(emptyA.equals(emptyB));
        assertFalse(emptyA.equals(emptyNull));
        assertFalse(emptyA.equals(nullNull));

        assertTrue(oneA.equals(oneAcopy));
        assertFalse(oneA.equals(emptyA));
        assertFalse(oneA.equals(emptyB));
        assertFalse(oneA.equals(emptyNull));
        assertFalse(oneA.equals(nullNull));

        assertTrue(nullA.equals(nullAcopy));
        assertFalse(nullA.equals(emptyA));
        assertFalse(nullA.equals(emptyB));
        assertFalse(nullA.equals(emptyNull));
        assertFalse(nullA.equals(nullNull));

        assertTrue(nullNull.equals(nullNullcopy));
    }

    @Test
    public void hashCodeContract() {
    }
}
