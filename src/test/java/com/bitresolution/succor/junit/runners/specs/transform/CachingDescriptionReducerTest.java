package com.bitresolution.succor.junit.runners.specs.transform;

import com.bitresolution.succor.junit.runners.specs.MockFixture;
import com.bitresolution.succor.junit.runners.specs.model.CaseFixture;
import com.bitresolution.succor.junit.runners.specs.model.Fixture;
import com.bitresolution.succor.junit.runners.specs.model.ScenarioFixture;
import com.bitresolution.succor.junit.runners.specs.model.SpecificationFixture;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.model.InitializationError;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.Method;
import java.util.Arrays;

import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Method.class)
public class CachingDescriptionReducerTest {

    private static final int HASH_CODE = 123456789;

    private static enum Case {
        NONE;
    }

    private DescriptionReducer reducer;
    private CachingDescriptionReducer cachingReducer;
    private Fixture mockedFixture;

    @Before
    public void setup() {
        reducer = mock(DescriptionReducer.class);
        cachingReducer = new CachingDescriptionReducer(reducer);

        mockedFixture = mock(Fixture.class);
    }

    @Test
    public void shouldFetchDescription() {
        //given
        Fixture fixture = new MockFixture();
        //when
        fixture.accept(cachingReducer);
        //then
        verify(reducer).visit(fixture);
    }

    @Test
    public void shouldFetchSpecificationDescription() throws InitializationError {
        //given
        SpecificationFixture fixture = new SpecificationFixture(Arrays.asList(mockedFixture), this.getClass());
        //when
        fixture.accept(cachingReducer);
        //then
        verify(reducer).visit(fixture);
    }

    @Test
    public void shouldFetchScenarioDescription() {
        //given
        final Method method = this.getClass().getEnclosingMethod();
        ScenarioFixture fixture = new ScenarioFixture(Arrays.asList(mockedFixture), method);
        //when
        fixture.accept(cachingReducer);
        //then
        verify(reducer).visit(fixture);
    }

    @Test
    public void shouldFetchCaseDescription() {
        //given
        final Method method = this.getClass().getEnclosingMethod();
        CaseFixture fixture = new CaseFixture(method, Case.NONE);
        //when
        fixture.accept(cachingReducer);
        //then
        verify(reducer).visit(fixture);
    }

    @Test
    public void shouldCacheDescription() {
        //given
        Fixture fixture = new MockFixture();
        fixture.accept(cachingReducer); //1st invocation
        //when
        fixture.accept(cachingReducer); //1st invocation should be cached
        //then
        verify(reducer).visit(fixture);
    }
}
