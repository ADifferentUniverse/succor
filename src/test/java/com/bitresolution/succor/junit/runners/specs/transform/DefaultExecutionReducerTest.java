package com.bitresolution.succor.junit.runners.specs.transform;

import com.bitresolution.succor.junit.runners.specs.MockFixture;
import com.bitresolution.succor.junit.runners.specs.model.CaseFixture;
import com.bitresolution.succor.junit.runners.specs.model.Fixture;
import com.bitresolution.succor.junit.runners.specs.model.ScenarioFixture;
import com.bitresolution.succor.junit.runners.specs.model.SpecificationFixture;
import examples.IntegerComparatorSpec;
import examples.SimpleSpec;
import examples.TwoScenarioSpec;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.model.InitializationError;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.*;

public class DefaultExecutionReducerTest {

    private ExecutionReducer reducer;
    private RunNotifier runNotifier;
    private DescriptionReducer descriptionReducer;

    @Before
    public void setup() {
        runNotifier = mock(RunNotifier.class);
        descriptionReducer = new DefaultDescriptionReducer();
        reducer = new DefaultExecutionReducer(descriptionReducer).with(runNotifier);

    }

    @Test
    public void shouldDoNothingForGenericTypeFixtures() {
        //given
        Fixture fixture = new MockFixture();

        //when
        fixture.accept(reducer);

        //then
        verifyZeroInteractions(runNotifier);
    }

    @Test
    public void shouldExecuteSpecWithOneScenario() throws InitializationError {
        //given
        final Method method = SimpleSpec.class.getDeclaredMethods()[0];
        final ScenarioFixture scenarioFixture = new ScenarioFixture(Collections.<Fixture>emptyList(), method);
        final SpecificationFixture specificationFixture = new SpecificationFixture(Arrays.asList(scenarioFixture), SimpleSpec.class);

        //when
        final Void actual = specificationFixture.accept(reducer);

        //then
        verify(runNotifier).fireTestStarted(scenarioFixture.accept(descriptionReducer));
        verify(runNotifier).fireTestFinished(scenarioFixture.accept(descriptionReducer));
        verifyNoMoreInteractions(runNotifier);
    }

    @Test
    public void shouldExecuteSpecWithOneCompoundScenario() throws InitializationError {
        //given
        final Method method = IntegerComparatorSpec.class.getDeclaredMethods()[0];
        final CaseFixture lessThanCase = new CaseFixture(method, IntegerComparatorSpec.IntegerComparatorCase.LESS_THAN);
        final CaseFixture equalsCase = new CaseFixture(method, IntegerComparatorSpec.IntegerComparatorCase.EQUAL);
        final CaseFixture greaterThanCase = new CaseFixture(method, IntegerComparatorSpec.IntegerComparatorCase.GREATER_THAN);
        final ScenarioFixture scenarioFixture = new ScenarioFixture(Arrays.asList(lessThanCase, equalsCase, greaterThanCase), method);
        final SpecificationFixture specificationFixture = new SpecificationFixture(Arrays.asList(scenarioFixture), SimpleSpec.class);

        //when
        final Void actual = specificationFixture.accept(reducer);

        //then
        verify(runNotifier).fireTestStarted(lessThanCase.accept(descriptionReducer));
        verify(runNotifier).fireTestFinished(lessThanCase.accept(descriptionReducer));
        verify(runNotifier).fireTestStarted(equalsCase.accept(descriptionReducer));
        verify(runNotifier).fireTestFinished(equalsCase.accept(descriptionReducer));
        verify(runNotifier).fireTestStarted(greaterThanCase.accept(descriptionReducer));
        verify(runNotifier).fireTestFinished(greaterThanCase.accept(descriptionReducer));
        verifyNoMoreInteractions(runNotifier);
    }

    @Test
    @Ignore
    public void shouldExecuteSpecWithScenarios() throws InitializationError {
        //given
        final Method[] method = TwoScenarioSpec.class.getDeclaredMethods();
        final ScenarioFixture simpleScenario = new ScenarioFixture(Collections.<Fixture>emptyList(), method[0]);
        final CaseFixture trueCase = new CaseFixture(method[1], TwoScenarioSpec.Cases.TRUE);
        final CaseFixture falseCase = new CaseFixture(method[1], TwoScenarioSpec.Cases.FALSE);
        final ScenarioFixture compoundScenario = new ScenarioFixture(Arrays.asList(trueCase, falseCase), method[1]);
        final SpecificationFixture specificationFixture = new SpecificationFixture(Arrays.asList(simpleScenario, compoundScenario), TwoScenarioSpec.class);

        //when
        final Void actual = specificationFixture.accept(reducer);

        //then
        verify(runNotifier).fireTestStarted(simpleScenario.accept(descriptionReducer));
        verify(runNotifier).fireTestFinished(simpleScenario.accept(descriptionReducer));
        verify(runNotifier).fireTestStarted(trueCase.accept(descriptionReducer));
        verify(runNotifier).fireTestFinished(trueCase.accept(descriptionReducer));
        verify(runNotifier).fireTestStarted(falseCase.accept(descriptionReducer));
        verify(runNotifier).fireTestFinished(falseCase.accept(descriptionReducer));
        verifyNoMoreInteractions(runNotifier);
    }
}
