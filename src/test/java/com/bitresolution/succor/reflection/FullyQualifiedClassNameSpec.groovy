package com.bitresolution.succor.reflection

import spock.lang.Specification

class FullyQualifiedClassNameSpec extends Specification {

    def "should parse fully qualified class names"() {
        expect:
        def fqcn = new FullyQualifiedClassName(fullname)
        assert fqcn.fullyQualifiedName == fullname
        assert fqcn.name == className
        assert fqcn.packageName.equals(new PackageName(packageName))
        assert fqcn.inDefaultPackage == isInDefaultPackage

        where:
        fullname           | getName  | packageName | isInDefaultPackage
        "BadClass"         | "BadClass" | ""          | true
        "java.lang.String" | "String"   | "java.lang" | false
    }
}

